" vim: set foldmethod=marker foldlevel=0 nomodeline:
augroup vimrc
  autocmd!
  autocmd BufWinEnter,Syntax * syn sync minlines=500 maxlines=500
augroup END

let s:darwin = has('mac')

" install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug
" ====================================================
call plug#begin()
  Plug 'vim-scripts/L9'
  Plug 'lifepillar/vim-solarized8'
  Plug 'bling/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'vim-scripts/FuzzyFinder'
  Plug 'easymotion/vim-easymotion'
  Plug 'airblade/vim-gitgutter'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-eunuch'
  Plug 'prabirshrestha/vim-lsp'
  Plug 'prabirshrestha/asyncomplete.vim'
  Plug 'prabirshrestha/asyncomplete-lsp.vim'
  Plug 'prabirshrestha/asyncomplete-buffer.vim'
  Plug 'prabirshrestha/asyncomplete-file.vim'
  Plug 'mattn/vim-lsp-settings'
  Plug 'rhysd/vim-lsp-ale'
  Plug 'tomtom/tcomment_vim'

  Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
  Plug 'junegunn/fzf.vim'
  Plug 'junegunn/goyo.vim'
  Plug 'junegunn/limelight.vim'
  Plug 'junegunn/rainbow_parentheses.vim'
  Plug 'junegunn/vim-easy-align'
  Plug 'junegunn/gv.vim'
  Plug 'junegunn/vim-after-object'
  Plug 'junegunn/vim-slash'
  Plug 'plasticboy/vim-markdown'
  Plug 'ConradIrwin/vim-bracketed-paste'
  Plug 'terryma/vim-multiple-cursors'
  Plug 'Yggdroot/indentLine'
  Plug 'itchyny/vim-cursorword'
  Plug 'JuliaLang/julia-vim'
  Plug 'brennier/quicktex'
  Plug 'lervag/vimtex'
  Plug 'w0rp/ale'
  Plug 'chaoren/vim-wordmotion'
call plug#end()

" syntax on
filetype plugin on
filetype indent on

" special syntax for Flash-X project
autocmd BufNewFile,BufRead *.F90-mc set filetype=fortran

" ====================================================
"
"                 BASIC SETUPS {{{
"
" ====================================================
set hidden
set mouse=a
set list
set laststatus=2
set lazyredraw
set nobackup
set nowritebackup
set updatetime=300
set hlsearch
set title titlestring=
set showcmd
set cmdheight=2
set signcolumn=yes
set clipboard+=unnamedplus
set foldlevel=20
set smartcase

set background=dark  " solarized dark
set termguicolors

set relativenumber
set number
set autoindent
set updatetime=100

set foldmethod=indent
set conceallevel=0  " reveal everything

" For vim-multiple-cursor this option is neccessary.
" ====================================================
set backspace=indent,eol,start

" Make tabs, trailing whitespaces visible
" ====================================================
set listchars=trail:·,tab:»»,nbsp:~

" tabs
" ====================================================
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab

" cursorlines
" ====================================================
set cursorline
highlight CursorLine ctermbg=232 cterm=none
highlight CursorLine guibg=#080808 gui=none

" Make the 81th column stands out
" ====================================================
" highlight ColorColumn ctermbg=Grey
call matchadd('ColorColumn', '\%133v', 100)

" let auto paste mode works fine with tmux
" ====================================================
let g:bracketed_paste_tmux_wrap = 0

augroup vimrc
  " if .txt file, render markdown highlighting
  " ====================================================
  autocmd BufRead,BufNewFile *.txt setlocal ft=markdown

  " spell check for
  " ====================================================
  autocmd FileType gitcommit setlocal spell
  autocmd FileType markdown setlocal spell
  autocmd FileType rst setlocal spell
  autocmd FileType txgt setlocal spell
  autocmd FileType tex setlocal spell

  " rst: tabsize == 3
  " ====================================================
  autocmd Filetype rst setlocal ts=3 sw=3 expandtab

  " python: tabsize == 4
  " ====================================================
  autocmd Filetype python setlocal ts=4 sw=4 expandtab

  " toml: tabsize == 4
  " ====================================================
  autocmd Filetype toml setlocal ts=4 sw=4 expandtab

  " fortran: tabsize == 3
  " ====================================================
  autocmd Filetype fortran setlocal ts=3 sw=3 expandtab
augroup END

" the leader is a comma
" ====================================================
let g:mapleader=','

" latex & markdowns
" ====================================================
let g:tex_flavor = 'tex'
let g:tex_conceal = ''
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 1
let g:vim_markdown_conceal_code_blocks = 0

" For Fortran syntax
" ====================================================
let g:fortran_do_enddo=1
let g:fortran_more_precise=1
let g:fortran_dialect='f95'
let g:fortran_free_source=1

" JSON
" ====================================================
let g:vim_json_conceal=0

" change cursor style in insert mode
" ====================================================
if s:darwin
  if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
  else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
  endif
else
  let &t_SI = "\e[5 q"
  let &t_EI = "\e[1 q"
endif
" }}}

" ====================================================
"
"                  KEY MAPS {{{
"
" ====================================================

" navigating buffers
" ====================================================
nnoremap ]b :bnext<cr>
nnoremap [b :bprev<cr>

" navigating tabs
" ====================================================
nnoremap ]t :tabn<cr>
nnoremap [t :tabp<cr>

" Movement in insert mode
" ====================================================
inoremap <C-h> <C-o>h
inoremap <C-l> <C-o>a
inoremap <C-j> <C-o>j
inoremap <C-k> <C-o>k
inoremap <C-^> <C-o><C-^>

" <tab> / <s-tab> Circular windows navigation
" ====================================================
nnoremap <tab>   <c-w>w
nnoremap <S-tab> <c-w>W

" Markdown headings
" ====================================================
nnoremap <leader>1 m`yypVr=`
nnoremap <leader>2 m`yypVr-``
nnoremap <leader>3 m`^i### <esc>`4l
nnoremap <leader>4 m`^i#### <esc>``5l
nnoremap <leader>5 m`^i##### <esc>`6l`

" Remove all trailing whitespace by pressing <leader>dw
" ====================================================
nnoremap <leader>dw :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" turn off serch highlight
" ====================================================
nnoremap <leader><space> :nohlsearch<CR>

" space key opens or closes folds
" ====================================================
nnoremap <space> za

" key mapping regular visual mode to visual Block mode
" ====================================================
nnoremap     v   <C-V>
nnoremap <C-V>      v

vnoremap     v   <C-V>
vnoremap <C-V>      v

" To navigate windows easier
" ====================================================
noremap <C-S-Down>  <C-W>j
noremap <C-S-Up>    <C-W>k
noremap <C-S-Left>  <C-W>h
noremap <C-S-Right> <C-W>l
"}}}

" ====================================================
"
"                PLUGIN OPTIONS {{{
"
" ====================================================

" For rainbow_parentheses
" ====================================================
autocmd VimEnter * RainbowParentheses
let g:rainbow#max_level = 16
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]

" For Goyo
" ====================================================
nnoremap <Leader>G :Goyo<CR>

function! s:goyo_enter()
  Limelight
endfunction

function! s:goyo_leave()
  Limelight!
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" TComment (not sure if it works for regular vim)
" ====================================================
map <C-/><C-/> :TComment<CR>
imap <C-/><C-/> <ESC>:TComment<CR>

" For EasyAlign
" ====================================================
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" For vim-after-object
" ====================================================
" eg) va= (visual after =)
autocmd VimEnter * call after_object#enable('=', ':', '-', '#', ' ')

" For Limelight
" ====================================================
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
let g:limelight_paragraph_span = 1
let g:limelight_priority = -1

" vim-lsp
" ====================================================
function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    let g:lsp_document_highlight_enabled = 0
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

let g:lsp_settings_root_markers = ['setup_call', '.git', '.git/']


" asyncomplete
" ====================================================
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

" register buffer
call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
    \ 'name': 'buffer',
    \ 'allowlist': ['*'],
    \ 'blocklist': ['go'],
    \ 'completor': function('asyncomplete#sources#buffer#completor'),
    \ 'config': {
    \    'max_buffer_size': 5000000,
    \  },
    \ }))

" file path
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'allowlist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))

" Airline
" ====================================================
let g:airline_highlighting_cache = 1
let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1

" FuzzyFinder
" ====================================================
map <C-o> :buffer \| FufFile<CR>

" fzf
" ====================================================
let g:fzf_layout = { 'down': '~40%' }

" maps for fzf
" ====================================================
nnoremap <leader>f :Files %:p:h<CR>
nnoremap <C-g> :Ag<CR>
nnoremap <C-l> :Lines<CR>
nnoremap <silent> <Leader><Enter>  :Buffers<CR>

" change :Ag with --follow option
command! -bang -nargs=*
  \ Ag call fzf#vim#ag(<q-args>, '--follow --nogroup --column --color',
                       \ fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}),
                       \<bang>0)

" For IndentLine
" ====================================================
let g:indentLine_enabled = 1
let g:indentLine_char = '┊'

" Ale
" ====================================================
let g:ale_fortran_gcc_options = '-Wall -cpp -fdefault-real-8 -fdefault-double-8'
let g:ale_virtualtext_cursor = 'current'

" style error => warning, style warning => warning
let g:ale_type_map = {'flake8': {'ES': 'W', 'WS': 'W'}}
let g:ale_linters = {'cpp': ['clangd'], 'python': ['ruff'], 'fortran': ['language_server']}
let g:ale_fixers = {'python': ['black']}

nmap ]a <Plug>(ale_next_wrap)
nmap [a <Plug>(ale_previous_wrap)

" vimtex
" ====================================================
source ~/.dotfiles/vim.quicktex/math_tex_keywords.vim
let g:vimtex_view_method='skim'
let g:vimtex_quickfix_mode=0
let g:vimtex_syntax_conceal_disable=1

let g:vimtex_compiler_progname = 'nvr'
nnoremap <leader>s :VimtexCompile<CR>
" }}}

" ====================================================


" solarized
" ====================================================
colorscheme solarized8
