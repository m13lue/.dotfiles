# Remove older command from the history if a duplicate is to be added.
setopt HIST_IGNORE_ALL_DUPS
# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}
# Set editor default keymap to emacs (`-e`) or vi (`-v`)
bindkey -v
# Set what highlighters will be used.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

#-------------------
# pure prompt # brew install pure   <-- do not use zimfw to install pure
#-------------------
autoload -U promptinit; promptinit
prompt pure

#-------------------
# zimfw
#-------------------
if [[ ${ZIM_HOME}/init.zsh -ot ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
  # Update static initialization script if it's outdated, before sourcing it
  source ${ZIM_HOME}/zimfw.zsh init -q
fi
source ${ZIM_HOME}/init.zsh

# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind up and down keys
zmodload -F zsh/terminfo +p:terminfo
if [[ -n ${terminfo[kcuu1]} && -n ${terminfo[kcud1]} ]]; then
  bindkey ${terminfo[kcuu1]} history-substring-search-up
  bindkey ${terminfo[kcud1]} history-substring-search-down
fi

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
#================================================================
# End configuration added by Zim install


# solarized dircolors for macOS
export LSCOLORS=gxfxbEaEBxxEhEhBaDaCaD

# faster vi mode
export KEYTIMEOUT=1


# Customize to your needs...
export VISUAL='vi'
export EDITOR='vi'

alias vi='nvim'

alias la='ls -lA'
alias ll='ls -lhv'
alias ls='ls -G'
alias notebook='jupyter notebook'
alias pasta='tmux new -s pasta'

alias flair='/Users/ylee/Documents/research/anl/flair/flair-3.1/flair'

###############################################################
##                         PATHs                             ##
###############################################################
# Export Homebrew path prior to system path
export PATH=/usr/local/bin:$PATH
# Export home .local
export PATH=~/.local/bin:$PATH

# export PATH=~/Library/Python/3.7/bin:$PATH

# ruby gem [user] path
# export GEM_PATH=~/.gem/ruby/2.6.0
# export PATH=$GEM_PATH/bin:$PATH

# path for SlugCode
export PATH=$PATH:/Users/ylee/Documents/research/SlugCode2/plotter/bin
# TAU
export PATH=$PATH:/Users/ylee/src/tau-2.28.1/apple/bin
# visit
export PATH=$PATH:/Applications/VisIt.app/Contents/Resources/bin
# FLUKA
export PATH=$PATH:/Users/ylee/Documents/research/anl/fluka/fluka4-2.2/bin
###############################################################


function fasd_cd {
  local fasd_ret="$(fasd -d "$@")"
  if [[ -d "$fasd_ret" ]]; then
    cd "$fasd_ret"
  else
    print "$fasd_ret"
  fi
}
alias j='fasd_cd -i'


#-------------------
# FZF
#-------------------
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_TMUX=1

function conda_init {
  # >>> conda initialize >>>
  # !! Contents within this block are managed by 'conda init' !!
  __conda_setup="$('/usr/local/Caskroom/miniconda/base/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
  if [ $? -eq 0 ]; then
      eval "$__conda_setup"
  else
      if [ -f "/usr/local/Caskroom/miniconda/base/etc/profile.d/conda.sh" ]; then
          . "/usr/local/Caskroom/miniconda/base/etc/profile.d/conda.sh"
      else
          export PATH="/usr/local/Caskroom/miniconda/base/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<
}

