# dotfiles

## Overview
neovim, zsh([zim](https://github.com/sorin-ionescu/prezto)) and tmux configurations

## Install
```sh
# install zsh, nvim and tmux
git clone
cd .dotfiles
sh ./install.sh
# reload zsh
zimfw install
```
