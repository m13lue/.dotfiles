#!/bin/bash
# modified from http://ficate.com/blog/2012/10/15/battery-life-in-the-land-of-tmux/

HEART='#'

if [[ `uname` == 'Linux' ]]; then
  current_charge=$(cat /proc/acpi/battery/BAT1/state | grep 'remaining capacity' | awk '{print $3}')
  total_charge=$(cat /proc/acpi/battery/BAT1/info | grep 'last full capacity' | awk '{print $4}')
else
  battery_info=`ioreg -rc AppleSmartBattery`
  current_charge=$(echo $battery_info | grep -o '"CurrentCapacity" = [0-9]\+' | awk '{print $3}')
  total_charge=$(echo $battery_info | grep -o '"MaxCapacity" = [0-9]\+' | awk '{print $3}')
  is_charging=$(echo $battery_info | grep -o '"IsCharging" = [A-Z]' | awk '{print $3}')
fi

charged_percent=$(echo "(($current_charge/$total_charge)*100)" | bc -l | cut -d '.' -f 1)
if [[ $charged_percent -gt 100 ]]; then
  charged_percent=100
fi

max_percent=100

charged_slots=$(echo "$charged_percent/20" | bc -l | cut -d '.' -f 1)

# actual print
echo -n '#[fg=colour196]'

if [[ $max_percent-$charged_percent -lt 5 ]] && [[ $is_charging == 'Y' ]]; then
    echo "[Charged]"
elif [[ $is_charging == 'Y' ]]; then
    echo "[Charging]"
else
    echo \[\=$charged_percent%\=\]
fi

# below is old $HEART version. but is not working properly.

# for i in `seq 1 $charged_slots`; do echo -n "$HEART"; done
#
# if [[ $charged_slots -lt 5 ]]; then
#   echo -n '#[fg=colour254]'
#   for i in `seq 1 $(echo "5-$charged_slots" | bc)`; do echo -n "$HEART"; done
# fi
