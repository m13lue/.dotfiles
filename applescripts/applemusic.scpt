if application "Music" is running then
  tell application "Music"
    set theName to name of the current track
    set theArtist to artist of the current track
    try
      return "♫  " & theName & " - " & theArtist
    on error err
    end try
  end tell
else
  return " - "
end if
