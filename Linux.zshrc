# Remove older command from the history if a duplicate is to be added.
setopt HIST_IGNORE_ALL_DUPS
# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}
# Set editor default keymap to emacs (`-e`) or vi (`-v`)
bindkey -v
# Set what highlighters will be used.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

#-------------------
# pure prompt
#-------------------
autoload -U promptinit; promptinit
prompt pure

#-------------------
# zimfw
#-------------------
if [[ ${ZIM_HOME}/init.zsh -ot ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
  # Update static initialization script if it's outdated, before sourcing it
  source ${ZIM_HOME}/zimfw.zsh init -q
fi
source ${ZIM_HOME}/init.zsh

# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind up and down keys
zmodload -F zsh/terminfo +p:terminfo
if [[ -n ${terminfo[kcuu1]} && -n ${terminfo[kcud1]} ]]; then
  bindkey ${terminfo[kcuu1]} history-substring-search-up
  bindkey ${terminfo[kcud1]} history-substring-search-down
fi

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
#================================================================
# End configuration added by Zim install


#-------------------
# key bindings
#-------------------
export VISUAL=vim
export EDITOR=vim
# neovim
alias vi='nvim'
alias ipynb='jupyter notebook'

# vim diff for neovim
alias vimdiff='nvim -d'

alias j='fasd_cd -i'


#-------------------
# path
#-------------------
export PATH=$PATH:~/.fzf/bin
export PATH=$PATH:$(ruby -e 'print Gem.user_dir')/bin
export PATH=$PATH:~/bin
export PATH=$PATH:/opt/visit/bin
export PATH=$PATH:~/.local/bin
# cuda
# export PATH=$PATH:/opt/cuda/bin
# export CUDA_HOME=/opt/cuda

# nvhpc
#export NVHPC_CUDA_HOME=/opt/nvidia/hpc_sdk/Linux_x86_64/22.11/cuda/11.8


# dircolors for Linux system
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

if type keychain >/dev/null 2>/dev/null; then
    keychain --nogui -q id_rsa
    [ -f ~/.keychain/${HOSTNAME}-sh ] && . ~/.keychain/${HOSTNAME}-sh
    [ -f ~/.keychain/${HOSTNAME}-sh-gpg ] && . ~/.keychain/${HOSTNAME}-sh-gpg
fi

#-------------------
# FZF
#-------------------
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_TMUX=1


(( ${+functions[module]} )) || source /etc/profile.d/modules.sh


# source ${SPACK_ROOT}/share/spack/spack-completion.bash
