#!/bin/sh
NVTEMP=$( nvidia-smi --format=nounits,csv,noheader --query-gpu=temperature.gpu | xargs echo )

if [[ $NVTEMP -gt 60 ]]; then
    echo "%{F#f00}GPU "$NVTEMP
else
    echo "GPU "$NVTEMP
fi
