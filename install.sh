#!/bin/bash
###################################

########## Variables
dir=$HOME/.dotfiles              # dotfiles directory
olddir=/tmp/oldDotFiles             # old dotfiles backup directory
rcFiles="vimrc tmux.conf zimrc"
diffFiles="zshrc"
depend="zsh tmux nvim"

# git setting
git config --global user.email "youngjunlee7@gmail.com"
git config --global user.name "Youngjun Lee"
git config --global core.editor nvim
git config --global merge.tool vimdiff

cd $HOME

# check dependencies
for dep in $depend; do
    if ! hash $dep 2>/dev/null; then
        echo -e "ERROR:" $dep "is not found."
        exit 1
    fi
done

# colorful (Solarized theme) file lists
wget --no-check-certificate https://raw.github.com/seebi/dircolors-solarized/master/dircolors.ansi-light
mv ~/.dircolors /tmp/
mv dircolors.ansi-light ~/.dircolors
eval `dircolors ~/.dircolors`

# install zim if not installed.
if ! hash zimfw 2>/dev/null; then
    # install zim
    curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh
else
    # found zim. upgrade it
    zimfw upgrade
fi

# change to dotfiles directory
cd $dir

# update submodules
echo -e "\033[7mUpdate git submodules...\033[0m"
git submodule init
git submodule update --init --recursive --remote
echo -e "\033[48;5;52mdone.\033[0m"

# create dotfiles_old in /tmp
mkdir -p $olddir

# move any existing dotfiles in $HOME to dotfiles_old directory,
# then create symlinks for $files in $HOME
echo -e "\033[7mMoving any existing dotfiles from $HOME to $olddir and creating symlinks...\033[0m"
for file in $rcFiles; do
    mv ~/.$file $olddir/$file
    ln -s $dir/$file ~/.$file
done
echo -e "\033[48;5;52mdone.\033[0m"

# make symlinks for system dependent files; $diffFiles
echo -e "\033[7mFor `uname` system...\033[0m"
for file in $diffFiles; do
    mv ~/.$file $olddir/$file
    ln -s $dir/`uname`.$file ~/.$file
done
echo -e "\033[48;5;52mdone.\033[0m"

